const db = require('../db/models/index');
const { User_history } = require('../db/models');


module.exports = {
    list: async (req, res) => {
       await User_history.findAll({
            order: [
                ["id", "ASC"]
            ],
            include: {
                model: db.User
            }
        }).then(historyall => {
            // res.send(historyall)
            res.status(200).render('user/show-history.ejs', { table: historyall })
        }).catch(err => {
            res.status(400).json({
                message: `Halaman view history All gak ketemu ${err}`
            })
        })
    },
    delete: async (req, res) => {
       await User.destroy({ where: { id: req.params.id } })
            .then(userdelete => {
                res.status(201).redirect('/user')
            })
    }
}