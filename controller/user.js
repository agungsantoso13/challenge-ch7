const db = require('../db/models/index');
const {
    User
} = require('../db/models');


module.exports = {
    list: async (req, res) => {
        await User.findAll({
                order: [
                    ["id", "ASC"]
                ],
                include: [{
                        model: db.UserProfile
                    },
                    {
                        model: db.User_history
                    },
                ]
            })
            .then(userall => {
                res.status(200).render('user/index.ejs', {
                    table: userall
                })
            })
            .catch(err => {
                res.status(400).json({
                    message: `Halaman view user All error  :${err}`
                })
            })
    },
    getById: async (req, res) => {
        await User.findOne({
                where: {
                    id: req.params.id
                },
                include: [{
                        model: db.UserProfile
                    },
                    {
                        model: db.User_history
                    },
                ]
            })
            .then(userId => {
                res.status(200).render('user/show-user', {
                    userId
                })
            })
            .catch(err => {
                res.status(400).json({
                    message: "Get user by ID tidak ada"
                })
            })
    },
    update: async (req, res) => {
        await User.update({
                user_name: req.body.user_name,
                password: req.body.password
            }, {
                where: {
                    id: req.params.id
                }
            })
            .then(user => {
                res.status(300).redirect('/user')
            }).catch(err => {
                res.status(400).json({
                    message: `Update user error : ${err}`
                })
            })
    },
    delete: async (req, res) => {
        await User.destroy({
                where: {
                    id: req.params.id
                }
            })
            .then(userdelete => {
                res.status(201).redirect('/user')
            }).catch(err => {
                res.status(400).json({
                    message: `delete user error : ${err}`
                })
            })
    },
    formCreate: (req, res) => {
        res.status(200).render('register.ejs')
    },
    updateById: async (req, res) => {
        await User.findByPk(req.params.id)
            .then(userupdate => {
                if (userupdate) {
                    res.status(200).render('user/update-user.ejs', {
                        user: userupdate
                    })
                } else {
                    res.status(400).json({
                        message: "User is Not Found"
                    })
                }
            }).catch(err => {
                res.status(400).json({
                    message: "gak masuk boss"
                })
            })
    }
}