const user = require('./user');
const userProfile = require('./userProfile');
const userHistory = require('./userHistory');



module.exports = {
    user,
    userProfile,
    userHistory
}; 