const db = require('../db/models/index');
const { UserProfile } = require('../db/models');


//belum error handling

module.exports = {
    list: async (req, res) => {
       await UserProfile.findAll({
            order: [
                ["id", "ASC"]
            ],
            include: {
                model: db.User
            }
        }).then(profileall => {
            res.status(200).render('user/show-profile.ejs', { table: profileall })
        }).catch(err => {
            res.status(400).json({
                message: `Halaman view user profile problem: ${err}`
            })
        })
    },
    getById: async (req, res) => {
       await UserProfile.findOne({
            where: { id: req.params.id }
        })
            .then(profileId => {
                res.status(200).render('user/detail-profile.ejs', { profileId })
            })
            .catch(err => {
                res.status(400).json({
                    message: "Halaman view profile by Id gak ketemu"
                })
            })
    },
    update: async (req, res) => {
        const { first_name, last_name, gender, email } = req.body;
       await UserProfile.update({ first_name, last_name, gender, email },
            {
                where: { id: req.params.id }
            })
            .then(profileupdate => {
                // res.send(profileupdate)
                res.status(300).redirect('/user')
            })
            .catch(err => {
                res.status(400).json({
                    message: "Halaman view profile by Id gak ketemu"
                })
            })
    },
    delete: async (req, res) => {
       await UserProfile.destroy({ where: { id: req.params.id } })
            .then(profiledelete => {
                res.status(300).redirect('/user')
            })
            .catch(err => {
                res.status(400).json({
                    message: "Halaman delete profile by Id gak ketemu"
                })
            })
    },
    updateById: async (req, res) => {
       await UserProfile.findByPk(req.params.id)
            .then(profileupdate => {
                if (profileupdate) {
                    res.status(200).render('user/update-profile.ejs', { profileupdate })
                } else {
                    res.status(400).json({
                        message: "User profile is Not Found"
                    })
                }
            }).catch(err => {
                res.status(400).json({
                    message: "gak masuk boss"
                })
            })
    }
}
