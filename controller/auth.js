const {
    User
} = require('../db/models');
const db = require('../db/models/index')
const bcrypt = require('bcrypt')


const dashboard = (req, res) => {
    res.status(200).render('dashboard.ejs')
}

const login = (req, res, next) => {
    User.authenticate(req.body)
        .then(user => {
            dataUser = {
                id: user.id,
                username: user.user_name,
                token: user.generateToken()
            }
            if (user.isAdmin) {
                res.status(300).redirect('/dashboard')
            }
            // res.status(300).redirect('/game')
            res.json(dataUser)
        })
        .catch(error => {
            next(error.message)
        })
};

const register = (req, res) => { // alias create
    const {
        user_name,
        password,
    } = req.body;

    const encryptedPassword = User.encrypt(password)

    User.create({
            user_name,
            password: encryptedPassword,
            UserProfile: {},
            User_histories: [{}]
        }, {
            include: [db.UserProfile, db.User_history]
        })
        .then(newUser => {
            // res.status(300).redirect('/login')
            res.status(200).json(newUser)
        }).catch((err) => next(err.message))
};

const showLogin = (req, res) => {
    let coba = {}; /*initial data kosong untuk render variable di file html login.ejs*/
    res.status(200).render('login.ejs', {
        message: coba
    });
};

module.exports = {
    dashboard,
    login,
    showLogin,
    register
}