const home = (req, res) => {
    res.render('pages/landing');
    res.status(200);
}

const game = (req, res) => {
    res.send('Masuk ke game boss');
    res.status(200);
}

module.exports = {
    home,
    game
};