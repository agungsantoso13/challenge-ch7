'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('UserProfiles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      first_name: {
        type: Sequelize.STRING,
        defaultValue: '',
        allowNull: false
      },
      last_name: {
        type: Sequelize.STRING,
        defaultValue: '',
        allowNull: false
      },
      gender: {
        type: Sequelize.STRING,
        defaultValue: '',
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        defaultValue: '',
        allowNull: false
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserProfiles');
  }
};