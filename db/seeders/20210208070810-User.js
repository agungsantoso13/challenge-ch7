'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users',
      [
        {
          id:"1",
          user_name: "agungs",
          password: "123456",
          isAdmin:"false",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id:"2",
          user_name: "sabrina",
          password: "1234567",
          isAdmin:"false",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id:"3",
          user_name: "vincentk",
          password: "12345678",
          isAdmin:"false",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id:"4",
          user_name: "admin",
          password: "12345678",
          isAdmin:"true",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {}
    );
  },


  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {
     
    });
  }
};

