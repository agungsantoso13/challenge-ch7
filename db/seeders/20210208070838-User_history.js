'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('User_histories',
      [{
        log: "Computer Win",
        userId: 1,
        log_date: new Date()
      },
      {
        log: "Player Win",
        userId: 2,
        log_date: new Date()
      },
      {
        log: "Player Win",
        userId: 3,
        log_date: new Date()
      }
      ], {});

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('User_histories', null, {
    
    });
  }
};