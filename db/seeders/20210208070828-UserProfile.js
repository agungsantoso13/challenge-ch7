'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('UserProfiles',
      [
        {
          first_name: "Agung",
          last_name: "Santoso",
          gender: "laki-laki",
          email: "agung@email.com",
          userId: "1"
        },
        {
          first_name: "Sabrina",
          last_name: "Amalia",
          gender: "perempuan",
          email: "sabrina@binar.co.id",
          userId: "2"
        },
        {
          first_name: "Vincent",
          last_name: "Kurniawan",
          gender: "laki-laki",
          email: "vincentk@cobacoba.com",
          userId: "3"
        }
      ], {});

  },


  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserProfiles', null, {
   
    });
  }
};