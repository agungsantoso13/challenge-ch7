'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_history.belongsTo(models.User, {
        foreignKey: {
          name: "userId"
        }
      })
    }
  };
  User_history.init({
    log: DataTypes.STRING,
    userId: DataTypes.INTEGER,
    log_date: {
      type:DataTypes.DATE,
      defaultValue:DataTypes.NOW
    }
  },
    {
      sequelize,
      modelName: 'User_history',
      timestamps:false
    });
  return User_history;
};