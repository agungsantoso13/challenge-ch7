'use strict';
const {
  Model
} = require('sequelize');
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.UserProfile, {
        foreignKey: {
          name: "userId"
        }
      });

      User.hasMany(models.User_history, {
        foreignKey: {
          name: "userId"
        }
      });
    }

    static encrypt(password) {
      return bcrypt.hashSync(password, 10)
    }

    checkPassword(password) {
      return bcrypt.compareSync(password, this.password)
    }

    static async authenticate({
      user_name,
      password
    }) {
      try {
        const user = await this.findOne({
          where: {
            user_name
          }
        })
        if (!user) {
          return Promise.reject("user not found");
        }
        const isPasswordValid = user.checkPassword(password)
        if (!isPasswordValid) {
          return Promise.reject("wrong password")
        }
        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error.message);
      }
    }

    generateToken() {
      const payload = {
        id: this.id,
        username: this.user_name
      }
      const rahasia = 'Rahasiaya'

      const token = jwt.sign(payload, rahasia)
      return token
    }

  };
  User.init({
    user_name: DataTypes.STRING,
    password: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};