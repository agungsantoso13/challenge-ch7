const passport = require('passport');
const {
    User
} = require('../db/models');
const {
    Strategy: JwtStrategy,
    ExtractJwt
} = require('passport-jwt')


const options = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'Rahasiaya'
}

const authenticate = async (payload, done) => {
    try {
        const user = await User.findByPk(payload.id)
        console.log("33333")
        console.log(user)
        return done(null, user)
    } catch (error) {
        return done(null, false, {
            message: error.message
        })
    }
}

passport.use(new JwtStrategy(options, authenticate))
module.exports = passport;