const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const errorHandler = require('./middleware/error');
const passport = require('passport');
const morgan = require('morgan');


//middleware
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(methodOverride('_method'))
app.use(cookieParser());
app.use(express.static('public'));


//Menggunakan Middleware passport
app.use(passport.initialize())

//template Engine
app.set("view engine", "ejs");

//import controller & router
const userRouter = require('./routes/user-router');
const userprofileRouter = require('./routes/user-profile-router');
const userhistoryRouter = require('./routes/user-history-router');
const router = require('./routes')



/*route start here*/
app.use(router);
app.use('/user', userRouter);
app.use('/user/profile', userprofileRouter);
app.use('/user/history', userhistoryRouter);


// 404 Handler
app.use(errorHandler.error404)
// internal server error
app.use(errorHandler.error500)

module.exports = app;