const express = require('express');
const router = express.Router();
const userHistoryController = require('../controller').userHistory;


router.get('/', userHistoryController.list);
// router.post('/', userHistoryController.create);
router.delete('/:id', userHistoryController.delete);

module.exports = router;