const express = require('express');
const router = express.Router();
const userController = require('../controller').user;


router.get('/', userController.list)
// router.post('/', userController.create)
router.get('/add', userController.formCreate)
router.get('/find/:id', userController.getById)
router.put('/find/:id', userController.update)
router.delete('/find/:id', userController.delete)
router.get('/update/:id', userController.updateById);



module.exports = router;