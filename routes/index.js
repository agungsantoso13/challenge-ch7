const express = require('express');
const router = express.Router();
const authCtrl = require("../controller/auth");
const homeCtrl = require("../controller/home");
const {
    restrict
} = require('../middleware/restrict');
const {
    superUser
} = require('../middleware/superUser');
const {
    user
} = require('../middleware/user');



router.get('/', homeCtrl.home);
router.get('/dashboard', restrict, superUser, authCtrl.dashboard); //superuser account
router.get('/game', restrict, user, homeCtrl.game) //user account

// API login
router.post('/login', authCtrl.login)
router.get('/login', authCtrl.showLogin)

// API register
router.post('/register', authCtrl.register)


module.exports = router;