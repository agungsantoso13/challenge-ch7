const express = require('express');
const router = express.Router();
const userProfileController = require('../controller').userProfile;


router.get('/', userProfileController.list);
// router.post('/', userProfileController.create)
router.get('/find/:id', userProfileController.getById);
router.put('/find/:id', userProfileController.update)
router.delete('/find/:id', userProfileController.delete)
router.get('/update/:id', userProfileController.updateById);
// router.get('/add', userProfileController.formCreate)


module.exports = router;