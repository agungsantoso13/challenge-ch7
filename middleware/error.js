const error404 = (req, res, next) => {
    const error = new Error("Page not found Boss!")
    error.status = 404;
    res.render("error404.ejs")
    next(error)
}

const error500 = (error, req, res, next) => {
    res.status(error.status || 500);
    res.render("error500.ejs", { error });
}

module.exports={
    error404,
    error500
}