const user = (req, res, next) => {
    if (req.user.id === req.params.id) {
        next();
    }
    res.status(400).json({
        message: "Please login as User to access"
    })
}

module.exports = {
    user
}