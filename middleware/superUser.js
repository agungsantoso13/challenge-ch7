const superUser = (req, res, next) => {
    if (req.user.username === "admin") {
        next()
    }
    res.status(400).json({
        message: "Please login as ADMIN to access"
    })
}

module.exports = {
    superUser
}